require('dotenv').config();

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const usersRouter = require('./routes/users');
const notesRouter = require('./routes/notes');
const authRouter = require('./routes/auth');
const port = process.env.PORT;

mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.on('open', () => console.log('Connected to Database'));
app.use(cors());
app.use(express.json());

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  { flags: 'a' },
);
app.use(morgan('tiny', { stream: accessLogStream }));


app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);

app.use((err, req, res, next) => {
  console.error(err);
  if (err.output) {
    return res.status(err.output.statusCode).send(err.output.payload);
  }
  res.status(500).send({ message: 'string' });
});

app.listen(port, () => console.log(`Example app listening on port ${port}`));
