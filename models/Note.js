const mongoose = require('mongoose');

const NoteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true
  }, 
  completed: {
    type: Boolean,
    default: false,
  }, 
  text: String,
  createdDate: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model('Notes', NoteSchema);

