const express = require('express');
const auth = require('../middleware/auth');
const {
  addNote,
  getNotesByUserId,
  getNoteById,
  updateNoteById,
  checkNoteById,
  deleteNoteById,
} = require('../controller/NotesController');
const router = express.Router();

// Getting User's notes
router.get('/', auth, getNotesByUserId);

// Adding Note for Users
router.post('/', auth, addNote);

// Getting User's note by id
router.get('/:id', auth, getNoteById);

// Updating User's note by id
router.put('/:id', auth, updateNoteById);

// Checking/Unchecking User's note by id
router.patch('/:id', auth, checkNoteById);

// Deletting User's note by id
router.delete('/:id', auth, deleteNoteById);

module.exports = router;
