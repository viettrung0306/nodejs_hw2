const express = require('express');
const router = express.Router();
const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Getting User's notes
router.post('/register', async (req, res) => {
  const body = req.body;

  if (!(body.username && body.password)) {
    return res.status(400).send({ message: 'string' });
  }

  await User.findOne({ username: body.username }).then((user) => {
    if (user) {
      res.status(400).send({ message: 'string' });
    }
  });

  const user = new User(body);

  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  const username = user.username;
  const token = jwt.sign(
    {
      user_id: user._id,
      username,
    },
    process.env.TOKEN_SECRET,
    {
      expiresIn: '2h',
    },
  );
  user.token = token;
  user.save().then((doc) => res.status(200).send(doc));
});

// Adding Note for Users
router.post('/login', async (req, res) => {
  const body = req.body;
  const user = await User.findOne({ username: body.username });

  if (user) {
    const username = user.username;
    const token = jwt.sign(
      {
        user_id: user._id,
        username,
      },
      process.env.TOKEN_SECRET,
      {
        expiresIn: '2h',
      },
    );
    const validatePassword = await bcrypt.compare(body.password, user.password);
    if (validatePassword) {
      res.status(200).send({ message: 'Success', user, accessToken: token });
    } else {
      res.status(400).send({ message: 'string' });
    }
  } else {
    res.status(400).send({ message: 'not user string' });
  }
});

module.exports = router;
