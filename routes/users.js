const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controller/UsersController');
// Getting User profile info
router.get('/', auth, getUser);

// Deleting User profile info
router.delete('/', auth, deleteUser);

// Changing user password
router.patch('/', auth, changeUserPassword);

module.exports = router;
