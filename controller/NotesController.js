const express = require('express');
const Note = require('../models/Note');

const getNotesByUserId = async (req, res) => {
  const userId = req.user._id;

  let limit = +req.query.limit || 0;
  let offset = +req.query.offset || 0;

  const notes = await Note.find({ userId }).limit(limit).skip(offset);
  res
    .status(200)
    .send({ offset: offset, limit: limit, count: notes.length, notes: notes });
};

const addNote = async (req, res) => {
  const userId = req.user._id;
  if (!userId) res.status(400).send({ message: 'string' });
  const notePayload = req.body;
  const note = new Note({ userId, ...notePayload });
  await note.save();
  res.status(200).send({ message: 'Success' });
};

const getNoteById = async (req, res) => {
  const userId = req.user._id;
  const noteId = req.params.id;
  const note = await Note.findOne({ _id: noteId, userId });

  if (!note) res.status(400).send({ message: 'string' });

  res.status(200).send({ note: note });
};

const updateNoteById = async (req, res) => {
  const noteId = req.params.id;
  const userId = req.user._id;
  const data = req.body;
  const note = await Note.findOne({ _id: noteId, userId });
  if (!note) res.status(400).send({ message: 'string' });
  await Note.findOneAndUpdate({ _id: noteId, userId }, { $set: data });
  res.status(200).send({ message: 'Success' });
};
const checkNoteById = async (req, res) => {
  const userId = req.user._id;
  const noteId = req.params.id;

  const note = await Note.findOne({ _id: noteId, userId });
  if (!note) return res.status(400).send({ message: 'string' });
  await Note.updateOne(
    { _id: noteId, userId },
    { $set: { completed: !note.completed } },
  );

  if (!note) {
    throw new InvalidRequestError('There is no note with such id!');
  }

  res.status(200).send({ message: 'Success' });
};

const deleteNoteById = async (req, res) => {
  const userId = req.user._id;
  const noteId = req.params.id;

  const note = await Note.findOne({ _id: noteId, userId });
  if (!note) return res.status(400).send({ message: 'string' });
  await Note.findOneAndRemove({ _id: noteId, userId });
  res.status(200).send({ message: 'Success' });
};

module.exports = {
  addNote,
  getNotesByUserId,
  getNoteById,
  updateNoteById,
  checkNoteById,
  deleteNoteById,
};
