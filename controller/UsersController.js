const User = require('../models/User');
const bcrypt = require('bcrypt');

const getUser = (req, res) => {
  const userInfo = req.user;
  res.status(200).json({ user: userInfo });
};

const deleteUser = async (req, res) => {
  const user = req.user;
  await User.deleteOne({ _id: user._id });
  res.status(200).send({ message: 'Success' });
};

const changeUserPassword = async (req, res) => {
  const user = req.user;
  const { oldPassword, newPassword } = req.body;
  if (user.password !== oldPassword)
    res.status(400).send({ message: 'String' });
  let newHashedPass = await bcrypt.hash(newPassword, 10);
  await User.updateOne(
    { _id: user._id },
    { $set: { password: newHashedPass } },
  );
  res.status(200).send({ message: 'Success' });
};

module.exports = { getUser, deleteUser, changeUserPassword };
